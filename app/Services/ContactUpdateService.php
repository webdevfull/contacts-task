<?php

namespace App\Services;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class ContactUpdateService extends BaseUpdateService
{
    /**
     * @return Model
     */
    protected function getBaseModel(): Model
    {
        return new Contact();
    }

    public function update()
    {
        return $this->getModel()->fill(
            $this->data
        )->saveOrFail();
    }
}
