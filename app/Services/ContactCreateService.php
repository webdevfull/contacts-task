<?php

namespace App\Services;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ContactCreateService extends BaseCreateService
{
    protected $data;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = $request->only(['name', 'surname', 'email', 'phone']);
    }

    public function getModel(): Model
    {
        return new Contact();
    }

    public function create()
    {
        $newContact = $this->getModel();
        $newContact->fill($this->data)->save();

        return $newContact;
    }
}
