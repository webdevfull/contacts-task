<?php

namespace App\Services;

use Illuminate\Http\Request;

abstract class BaseCreateService implements ServiceInterface
{
    protected $data;

    public function __construct(Request $request)
    {
        $this->data = $request->all();
    }

    abstract public function create();

    public function run()
    {
        return $this->create();
    }
}
