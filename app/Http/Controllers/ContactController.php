<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactCreateRequest;
use App\Http\Requests\ContactUpdateRequest;
use App\Models\Contact;
use App\Services\ContactCreateService;
use App\Services\ContactUpdateService;
use Illuminate\Http\JsonResponse;

class ContactController extends Controller
{
    /**
     * Display a listing of the contact.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(Contact::query()->orderByDesc('created_at')->get());
    }

    /**
     * Store a newly created contact in storage.
     *
     * @param ContactCreateRequest $request
     * @return JsonResponse
     */
    public function store(ContactCreateRequest $request)
    {
        try {
            $newContact = (new ContactCreateService($request))->run();
            return response()->json(['success' => true, 'message' => 'Contact created successfully', 'contact' => $newContact]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], 400);
        }
    }

    /**
     * Display the specified contact.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        try {
            $contact = Contact::findOrFail($id);
            return response()->json(['success' => true, 'contact' => $contact]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], 404);
        }
    }

    /**
     * Update the specified contact in storage.
     *
     * @param ContactUpdateRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(ContactUpdateRequest $request, $id)
    {
        try {
            $contact = (new ContactUpdateService($id, $request))->run();
            return response()->json(['success' => true, 'message' => 'Contact updated successfully', 'contact' => $contact]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], 400);
        }
    }

    /**
     * Remove the specified contact from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            Contact::findOrFail($id)->delete();
            return response()->json(['success' => true, 'message' => 'Contact deleted!']);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()]);
        }
    }
}
