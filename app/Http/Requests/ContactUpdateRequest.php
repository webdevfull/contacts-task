<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class ContactUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|alpha_dash',
            'surname' => 'nullable|max:255|alpha_dash',
            'email' => 'required|email|unique:contacts,email,'. request()->get('id'),
            'phone' => ['required', new PhoneNumber()],
        ];
    }
}
