## Contacts CRUD with Laravel + VueJs

### Required commands to run the project

- cp .env.example .env  (Linux)
- copy .env.example .env (Windows)
  

- composer install <br/>
- php artisan key:generate <br/>
  

- npm install
- npm run dev


#### first create database and set db config variables to .env file, then run
- php artisan migrate
  

#### at the end run the following command and visit with this link **[http://127.0.0.1:8000/](http://127.0.0.1:8000/)**
- php artisan serve
