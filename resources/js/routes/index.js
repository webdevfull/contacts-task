import AllProduct from '../components/Contact/ListContact.vue';
import CreateProduct from '../components/Contact/CreateContact.vue';
import EditProduct from '../components/Contact/EditContact.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllProduct
    },
    {
        name: 'create',
        path: '/create',
        component: CreateProduct
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditProduct
    }
];
